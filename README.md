# All Worlds Editor Utility Widget
All World Editor Utility Widget is a simple example project that showcases an Editor Utility Widget capable of editing actors in a level both in Editor and PIE.

## Basics: How to use
- Open the "TestLevel" map
- Right click on "EUW_ActorScaler" and select "Run Editor Utility Widget"
- Use the widget to edit the scale of the actor in the scene.
- Hit play and keep editing stuff (hint: press Shift+F1 to see the cursor again!)
- Close PIE and see all the changes did in PIE are kept!

## Most important classes and assets
### Widgets
- **EUW_AllWorldsUtility**: this editor utility widget defines the life cycle for children to inherit. The most important function is OnWorldChanged, which provides a simple hook to update any references when PIE begins or ends.
- **EUW_ActorScaler**: simple test tool. It allows real-time editing of the scale of any actor marked with a specific Actor tag.